###############################################################################
# Base image for all containers to build upon.
FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/python:3.11-slim AS base

WORKDIR /usr/src/app

RUN pip install --no-cache-dir poetry && poetry self add poetry-plugin-export

###############################################################################
# Just enough to run tox. Tox will install other dependencies needed to run
# tests.
FROM base AS tox

RUN pip install tox

ENTRYPOINT ["tox"]
CMD []

###############################################################################
# Intall dependencies
FROM base AS installed-deps

# Install specific requirements for the package.
COPY pyproject.toml poetry.lock ./
RUN poetry config virtualenvs.create false && \
  poetry install --no-root --without=dev

###############################################################################
# Full application image
FROM installed-deps AS full
# Copy application source and install it. Use "-e" to avoid needlessly copying
# files into the site-packages directory.
ADD ./ ./
RUN poetry install --without=dev

ENTRYPOINT ["cardclient"]
